package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Product;
import com.app.service.IProductService;

@RestController // @Controller cls level annotation + @ResponseBody : added on ret type of ALL
				// request handling methods : annotated via : @RequestMapping , @GetMapping ...
@RequestMapping("/products")
@CrossOrigin
public class ProductController {
	// dependency : service layer i/f
	@Autowired
	private IProductService productService;

	public ProductController() {
		System.out.println("in ctor of " + getClass().getName());
	}

	// add a req handling method to return representation of list of available
	// products
	@GetMapping
	public ResponseEntity<?> fetchAllProducts() {
		System.out.println("in fetch all products");
		List<Product> products = productService.getAllProducts();
		// chk if empty
		if (products.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		// non empty list
		return new ResponseEntity<>(products, HttpStatus.OK);
	}

	// add a req handling method to return representation of selected product by id
	// OR in case of invalid id
	// SC 404
	@GetMapping("/{pid}")
	public ResponseEntity<?> getProductDetails(@PathVariable int pid) {
		System.out.println("in get product dtls " + pid);
		try {
			return ResponseEntity.ok(productService.getProductDetails(pid));
		} catch (RuntimeException e) {
			System.out.println("err in controller " + e);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	// add new REST end point : to fetch product by name
	@GetMapping("/name/{pName}")
	public ResponseEntity<?> getProductDetailsByName(@PathVariable String pName) {
		System.out.println("in get prd by name " + pName);
		try {
			return ResponseEntity.ok(productService.getProductDetailsByName(pName));
		} catch (RuntimeException e) {
			System.out.println("err in controller " + e);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	// add new REST end point : to add a product
	@PostMapping
	public ResponseEntity<?> addNewProduct(@RequestBody Product p) {
		System.out.println("in add new product " + p);
		return ResponseEntity.ok(productService.addProductDetails(p));
	}

	// add new REST end point : to update existing product
	@PutMapping
	public ResponseEntity<?> updateProductDetails(@RequestBody Product p) {
		System.out.println("in update product " + p);
		try {
			return ResponseEntity.ok(productService.updateProductDetails(p));
		} catch (RuntimeException e) {
			System.out.println("err in controller " + e);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	//add REST end point to delete product details
	@DeleteMapping("/{pid}")
	public void deleteProductDetails(@PathVariable int pid) {
		System.out.println("in del product dtls " + pid);
		try {
			productService.deleteProductDetails(pid);
			
		} catch (RuntimeException e) {
			System.out.println("err in controller " + e);
		}
		
	}

}
