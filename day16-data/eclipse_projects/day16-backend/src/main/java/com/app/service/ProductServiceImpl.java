package com.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ProductNotFoundException;
import com.app.dao.ProductRepository;
import com.app.pojos.Product;

@Service // mandatory
@Transactional // optional since it's by default already added on JpaRepository
public class ProductServiceImpl implements IProductService {
	// dependency
	@Autowired
	private ProductRepository productRepo;

	@Override
	public List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return productRepo.findAll();
	}

	@Override
	public Product getProductDetails(int productId) {
		// invoke dao's method
		Optional<Product> optionalProduct = productRepo.findById(productId);
		if (optionalProduct.isPresent())
			return optionalProduct.get();
		// if product is not found : throw custom exception
		throw new ProductNotFoundException("Product Not Found : Invalid ID " + productId);
	}

	@Override
	public Product getProductDetailsByName(String pName) {
		// invoke repo's method
		Optional<Product> optional = productRepo.findByName(pName);
		if (optional.isPresent())
			return optional.get();
		// if product is not found : throw custom exception
		throw new ProductNotFoundException("Product Not Found : Invalid Product Name " + pName);

	}

	@Override
	public Product addProductDetails(Product p) {
		// TODO Auto-generated method stub
		return productRepo.save(p);
	}// auto dirty chking --insert query --L1 cache destroyed --cn rets cn pool
		// --persistence ctx is closed
		// rets detached POJO to the caller(REST controller)

	@Override
	public Product updateProductDetails(Product p) {
		// chk if product exists
		Optional<Product> optional = productRepo.findById(p.getProductId());
		if (optional.isPresent())
			return productRepo.save(p); // update
		// if product is not found : throw custom exception
		throw new ProductNotFoundException("Product Not Found : Invalid Product id " + p.getProductId());

	}
	// auto dirty chking --update query --L1 cache destroyed --cn rets cn pool
	// --persistence ctx is closed
	// rets detached POJO to the caller(REST controller)

	@Override
	public void deleteProductDetails(int productId) {
		// chk if product exists : yes : delete , otherwise throw exc.
		Optional<Product> optional = productRepo.findById(productId);
		if (optional.isPresent())
			productRepo.deleteById(productId);
		else
			// if product is not found : throw custom exception
			throw new ProductNotFoundException("Product Not Found : Invalid Product id " + productId);

	}

}
