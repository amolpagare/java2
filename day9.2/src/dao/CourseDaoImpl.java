package dao;

import pojos.Course;
import static utils.HibernateUtils.getSf;
import org.hibernate.*;

public class CourseDaoImpl implements ICourseDao {

	@Override
	public String launchCourse(Course c) {
		String mesg="Launching course failed...";
		//session
		Session session=getSf().getCurrentSession();
		//tx
		Transaction tx=session.beginTransaction();
		try {
			//c : transient
			session.persist(c);//persistent 
			tx.commit();//dirty chking : insert , session closed
			mesg="Launched course with course id "+c.getCourseId();
		}catch (RuntimeException e) {
			if(tx != null)
				tx.rollback();
			throw e;
		}
		return mesg;
	}

}
