package dao;

import java.sql.*;
import static utils.DBUtils.getDBConnection;

import pojos.Voter;

public class VoterDaoImpl implements IVoterDao {
	private Connection cn;
	private PreparedStatement pst1;

	public VoterDaoImpl() throws Exception {
		cn = getDBConnection();
		pst1 = cn.prepareStatement("select * from voters where email=? and password=?");
		System.out.println("voter dao created ....");
	}

	@Override
	public Voter authenticateVoter(String email, String pass) throws SQLException {
		// set IN params
		pst1.setString(1, email);
		pst1.setString(2, pass);
		try (ResultSet rst = pst1.executeQuery()) {
			if (rst.next())
				return new Voter(rst.getInt(1), rst.getString(2), email, pass, rst.getBoolean(5), rst.getString(6));
		}
		return null;
	}

	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		if (cn != null)
			cn.close();
		System.out.println("voter dao cleaned");
	}

}
