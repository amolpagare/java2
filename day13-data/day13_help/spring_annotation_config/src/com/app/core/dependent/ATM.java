package com.app.core.dependent;

public interface ATM {
   void deposit(double amt);
   void withdraw(double amt);
}
