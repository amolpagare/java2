package com.app.core.dependency;

public interface Transport {
  void informBank(byte[] data);
}
