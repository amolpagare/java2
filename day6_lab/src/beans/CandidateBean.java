package beans;

import java.sql.SQLException;
import java.util.List;

import dao.CandidateDaoImpl;
import pojos.Candidate;

public class CandidateBean {
	//properties
	private CandidateDaoImpl candidateDao;
	//clnt's request parameter : chosen candidate id : int (WC :setter : parsing)
	private int cid;
	//constr : arg less constr.
	public CandidateBean() throws Exception{
		System.out.println("in candidate bean");
		//create dao instance
		candidateDao=new CandidateDaoImpl();
	}
	//no getters n setters are required for dao since it's an internal property : meant only for bean
	
	//B.L : to fetch list of all candidates
	public List<Candidate> getCandidates()  throws SQLException
	{
		System.out.println("in B.L candidate bean ");
		return candidateDao.getAllCandidates();
	}
	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	//B.L method to increment selected candidate votes 
	public String updateVotes() throws SQLException
	{
		System.out.println("in B.L cid="+cid);
		//invoke candidate dao's method 
		return candidateDao.incrementVotes(cid);
	}
	
	//add B.L method to clean up candidate  dao
		public void daoCleanUp() throws SQLException
		{
			candidateDao.cleanUp();
		}
		

}
