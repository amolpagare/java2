package tester;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import dependent.ATMImpl;

public class TestSpring {

	public static void main(String[] args) {
		// start spring container in Java app : using xml based instrs stored under runtime class path
		//o.s.c.s.ClassPathXmlApplicationContext : class
		//BeanFactory <----- ApplicationContext <-----ClassPathXmlApplicationContext
		try(ClassPathXmlApplicationContext ctx=new ClassPathXmlApplicationContext("spring-config.xml"))
		{
			System.out.println("SC booted...");
			//1st demand : tell SC to supply located--loaded--inst(def constr) --D.I => ready to use bean
			//API : o.s.b.BeanFactory : T getBean(String beanId,Class<T> beanClass) throws BeansException
			ATMImpl atm1 = ctx.getBean("atm_bean",ATMImpl.class);//1st demand
			//B.L
			atm1.withdraw(1000);
			ATMImpl atm2 = ctx.getBean("atm_bean",ATMImpl.class);
			ATMImpl atm3 = ctx.getBean("atm_bean",ATMImpl.class);
			System.out.println(atm1==atm2);
			System.out.println(atm1==atm3);
		}catch (Exception e) {
			e.printStackTrace();
		}

	}

}
