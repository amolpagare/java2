package com.app.service;

import java.util.List;

import com.app.pojos.Vendor;

public interface IVendorService {
	Vendor authenticateUser(String email, String password);

	// add a method to list all vendors
	List<Vendor> listAllVendors();

	// add a method to delete vendor details
	String deleteVendorDetails(int vendorId);
	//add a method to register new vendor
		String registerNewVendor(Vendor vendor);//i/p : transient POJO
}
