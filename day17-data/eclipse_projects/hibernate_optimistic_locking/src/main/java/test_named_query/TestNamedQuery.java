package test_named_query;

import static utils.HibernateUtils.getSf;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import pojos.Stock;
//Test named query
public class TestNamedQuery {

	public static void main(String[] args) {
		try (SessionFactory sf = getSf()) {
			Session hs = sf.getCurrentSession();
			Transaction tx = hs.beginTransaction();
			try {
				System.out.println("All stocks having quantity > 250");
				// invoking a named query : which is pre compiled & ready to use
				hs.createNamedQuery("Stock.findByQuantity", Stock.class).setParameter("qty", 250).getResultList()
						.forEach(System.out::println);
				tx.commit();
			} catch (RuntimeException e) {
				tx.rollback();
				throw e;
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
