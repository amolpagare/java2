package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Role;
import com.app.pojos.Vendor;

@Repository // spring bean : data access logic
public class VendorDaoImpl implements IVendorDao {
	// DAO : dependent obj , dependency :
	// javax.persistence.EntityManager (equivalent to org.hibernate.Session)
//	@PersistenceContext
	@Autowired
	private EntityManager manager;

	@Override
	public Vendor authenticateUser(String email, String password) {
		Vendor v = null;
		String jpql = "select v from Vendor v  where v.email=:em and v.password=:pass";

		v = manager.createQuery(jpql, Vendor.class).setParameter("em", email).setParameter("pass", password)
				.getSingleResult();

		return v;// dao layer is returning PERSISTENT vendor pojo to service
	}

	@Override
	public List<Vendor> listAllVendors() {
		String jpql = "select v from Vendor v where v.userRole=:role";
		return manager.createQuery(jpql, Vendor.class).setParameter("role", Role.VENDOR).getResultList();
	}

	@Override
	public String deleteVendorDetails(int vendorId) {
		String mesg="Vendor deletion failed";
		// get persistent POJO ref from it's id :
		// public T find(T class , Object uniqueId)
		Vendor v = manager.find(Vendor.class, vendorId);
		// chk null
		if (v != null) {
			// v : PERSISTENT
			// use EntityManager : public void remove(Object persistencePojoRef)
			manager.remove(v);
			mesg="Vendor deleted successfully....";
		}
		return mesg;
	}

	@Override
	public String registerNewVendor(Vendor vendor) {
		//vendor : TRANSIENT
		manager.persist(vendor); //vendor : PERSISTENT : part of L1 cache 
		return "Vendor registered successfully";
	}

	@Override
	public Vendor getVendorDetails(int vendorId) {
		// TODO Auto-generated method stub
		return manager.find(Vendor.class, vendorId);
	}

	@Override
	public String updateVendor(Vendor vendor) {
		manager.merge(vendor);//re attaching detached POJO to L1 cache , making it persistent.
		return "Vendor details updated...";
	}
	
	
	

}
