package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@DiscriminatorValue(value = "mgr")
@PrimaryKeyJoinColumn(name = "emp_id")
public class Manager extends Employee {
	private double bonus;
	@Column(name = "dept_name",length = 30)
	private String deptName;
	public Manager() {
		// TODO Auto-generated constructor stub
	}
	public double getBonus() {
		return bonus;
	}
	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	
}
