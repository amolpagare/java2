package com.app.pojos;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@DiscriminatorValue("worker")
@PrimaryKeyJoinColumn(name = "emp_id")
public class Worker extends Employee {
	@JsonProperty("hours")
	private int noOfHours;
	@JsonProperty("hrly_rate")
	private double rate;
	public int getNoOfHours() {
		return noOfHours;
	}
	public void setNoOfHours(int noOfHours) {
		this.noOfHours = noOfHours;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	
}
