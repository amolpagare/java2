package com.app.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {
	//using a named query : method arg name here HAS to match with query parameter name :nm
	Optional<Product>findByName(String nm);

}
