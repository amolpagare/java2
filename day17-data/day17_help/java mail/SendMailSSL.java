package java_mail;

import java.util.Properties;
import java.util.Scanner;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

class Mailer {
	public static void send(String from, String password, String to, String sub, String msg) throws Exception {
		// Get properties object
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.debug", "true");
		// get Session
		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		});
		// compose message

		MimeMessage message = new MimeMessage(session);
		message.addRecipient(Message.RecipientType.TO,
				new InternetAddress(to));
		message.setSubject(sub);
		message.setText(msg);
		// send message
		Transport.send(message);
		System.out.println("message sent successfully");

	}
}

public class SendMailSSL {
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter your email , password");
			String fromEmail = sc.next();
			String pass = sc.nextLine();
			
			System.out.println("Enter recipient(to) email ");
			String to = sc.nextLine();
			System.out.println("Enter mail subject");
			String subject = sc.nextLine();
			System.out.println("Enter mail mesg");
			String mesg = sc.nextLine();
			Mailer.send(fromEmail, pass, to, subject, mesg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}