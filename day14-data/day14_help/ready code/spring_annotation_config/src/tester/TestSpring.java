package tester;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.app.core.dependent.ATM;


public class TestSpring {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext("com.app.core")) {
			System.out.println("SC started");
			ATM ref = ctx.getBean("my_atm", ATM.class);
			ref.deposit(100);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
