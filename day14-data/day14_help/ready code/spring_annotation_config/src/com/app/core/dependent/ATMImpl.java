package com.app.core.dependent;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.app.core.dependency.Transport;

@Component("my_atm")
public class ATMImpl implements ATM {
	@Autowired
	@Qualifier("testTransport")
	private Transport myTransport;
	@Value("${cash.value}")
	private double cash;

	public ATMImpl() {
		System.out.println("in constr of " + getClass().getName() + " " + myTransport);
	}

	@Override
	public void deposit(double amt) {
		System.out.println("depositing " + amt);
		byte[] data = ("depositing " + amt).getBytes();
		myTransport.informBank(data);

	}

	@Override
	public void withdraw(double amt) {
		System.out.println("withdrawing " + amt);
		byte[] data = ("withdrawing " + amt).getBytes();
		myTransport.informBank(data);
	}

	// init style method
	@PostConstruct
	public void myInit() {
		System.out.println("in init " + myTransport+" cash="+cash);
	}

	// destroy style method
	@PreDestroy
	public void myDestroy() {
		System.out.println("in destroy");
	}

}
