package com.app.core.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:app.properties")
public class Config {
	public Config() {
		System.out.println("config constr");
	}
}
